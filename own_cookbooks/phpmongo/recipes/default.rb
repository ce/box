#
# Cookbook Name:: phpredis
# Recipe:: default
#
# Copyright 2012, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
script "install_mogodb_driver" do
	interpreter "bash"
	user "root"
	cwd "/tmp"
	code <<-EOH
		pecl install mongo
	EOH
end

cookbook_file "/etc/php5/conf.d/mongo.ini" do
	source "mongo.ini"
	mode "0644"
end
