include_recipe "apache2"

web_app "lmp" do
    server_name "lmp.box.pit-dev.com"
    docroot "/var/www/lmp"
end

web_app "dmp" do
    server_name "dmp.box.pit-dev.com"
    docroot "/var/www/dmp"
end

web_app "sym" do
    server_name "sym.box.pit-dev.com"
    docroot "/var/www/sym"
end

web_app "totalbox" do
    server_name "totalbox.box.pit-dev.com"
    docroot "/var/www/totalbox/web"
    allow_override "All"
end

web_app "cfbb_totalbox_io" do
    server_name "cfbb.totalbox.io"
    docroot "/var/www/totalbox/web"
    allow_override "All"
end

web_app "adserver" do
	server_name "adserver.box.pit-dev.com"
	docroot "/var/www/adserver"
end

web_app "phpmyadmin" do
	server_name "pma.box.pit-dev.com"
	docroot "/opt/phpmyadmin"
end