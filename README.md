# How to use:

If you do not have **librarian** installed install it now

	 gem install librarian-chef

Afterwards you can download all cookbooks

	librarian-chef install

And fire up the VM

	vagrant up

Finish up the installation

	composer install
	./install dev
